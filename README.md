<h1 align="center">
  <br>
  <a href="https://gitlab.com/franciscomoli/wordpress_deployment"><img src="https://github.com/kubernetes/kubernetes/blob/master/logo/logo.svg" alt="Wordpress MySQL  Deployment" width="200"></a>
  <br>
  Deployment Wordpress MySQL Kubernetes 
  <br>
</h1>

<h4 align="center">Video Tutorial <a href="https://kubernetes.io/docs/tasks/manage-kubernetes-objects/declarative-config/" target="_blank">Link</a>.</h4>
<h4 align="center">Kubernetes Object <a href="https://kubernetes.io/docs/tasks/manage-kubernetes-objects/declarative-config/" target="_blank">Link</a>.</h4>


<p align="center">

</p>

<p align="center">
  <a href="#hpre-requisites">Pre - Requisite</a> •
  <a href="#how-to-use">How To Use</a> •
  <a href="#download">Download</a> •
  <a href="#credits">Credits</a> •
  <a href="#related">Related</a> •
  <a href="#license">License</a>
</p>

[comment]: <> ([screenshot]&#40;https://raw.githubusercontent.com/.gif&#41;)

## Pre - Requisite

```bash
#Install Kubectl Debian - Ubuntu
#Update the apt package index and install packages needed to use the Kubernetes apt repository:
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl
#Download the Google Cloud public signing key:
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
#Add the Kubernetes apt repository:
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
#Update apt package index with the new repository and install kubectl:
sudo apt-get update
sudo apt-get install -y kubectl

#intall Kubectl con snap 
snap install kubectl --classic
kubectl version --client

# Install kubectl bin client
#Download kubectl
curl -LO https://dl.k8s.io/release/v1.21.0/bin/linux/amd64/kubectl


#Install doctl Digital Ocean command line tool
#Donwload doctl
curl -OL https://github.com/digitalocean/doctl/releases/download/v1.32.2/doctl-\1.32.2-linux-amd64.tar.gz

# extract bin doctl
tar xf doctl-1.32.2-linux-amd64.tar.gz
# move bin 
sudo mv ~/doctl /usr/local/bin
# test install
doctl
```

## How To Use

### Objectives
- Create PersistentVolumeClaims and PersistentVolumes
- Create a kustomization.yaml with 
  - a Secret generator
  - MySQL resource configs
  - WordPress resource configs
- Apply the kustomization directory by kubectl apply -k ./



```bash
# Clone this repository
git clone https://gitlab.com/franciscomoli/wordpress_deployment
# go to project
cd wordpress_deployment
#create your kustomization with your secret pasword for mysql , and deployment
cat <<EOF >./kustomization.yaml
resources:
  - mysql-deployment.yaml
  - wordpress-deployment.yaml
secretGenerator:
- name: mysql-pass
  literals:
  - password=YOUR_PASSWORD
EOF
#add kube config and doctl(Digital Ocean) 
doctl kubernetes cluster kubeconfig save 291cf77a-61c8-....

#select you cluster (MY_CONTEXT) and namespace
kubectl config use-context MY_CONTEXT
#create namespace TEST
kubectl create ns TEST
#You can apply the directory by , -k is kustomization
kubectl apply -k ./
#Verify that the Secret exists by running the following command:

```

Note: If you're using Linux, [see this guide](https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume).


## Download

You can [download](https://github.com/digitalocean/doctl#use-with-kubectl)

curl -OL https://github.com/digitalocean/doctl/releases/download/v1.32.2/doctl-\1.32.2-linux-amd64.tar.gz
the latest installable version of Markdownify for Windows, macOS and Linux.


## Credits

This software uses the following open source packages:

- [Kubernetes Doc mysql Wordpress](https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume/)
- [Kubernetes Doc kustomization](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization)
- [Kubernetes Doc Kubernetes Object](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/declarative-config/)



## Related



## Support

Kubernetes 1.9 or later


## License

MIT


